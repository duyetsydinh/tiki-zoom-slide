

$(document).ready(function () {
    $(".product-slide").slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [{
 
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
   
      }, {
   
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          dots: true
        }
   
      }, {
   
        breakpoint: 300,
        slidesToShow: 3,
   
      }]
     
    });
  });
//function slide images

  $(document).ready(function () {
   
    $(".product-slide img").click(function(){
      $(".product-slide img.active").removeClass("active");
      let image_path =  $(this).attr("src");
      
      $(".image__wrap--image").attr("src", image_path );
     
      $(this).addClass("active");
     
 $(".img-zoom-result").css("background-image", "url(" + image_path + ")");
 
    })

  });

  // function to choose and zoom image 